#!/bin/sh

Usage() {
    echo ""
    echo "Usage"
    echo "neonatal-PSMD.sh input"
    echo ""
    echo "Example"
    echo "neonatal-PSMD.sh *_L1.nii.gz"
    echo ""
    echo "This script creates all the files needed for TBSS and PSMD analysis in the neonatal brain"
    echo ""
    echo "Script created by Manuel Blesa"
    echo ""
    exit
}
[ "$1" = "" ] && Usage

mkdir initial_files/

IMAGESET=${*}
for IMG in $IMAGESET
	do

	BASENAME=` basename ${IMG} | cut -d '_' -f 1 `
	echo ""
        echo "Converting subject ${BASENAME} from FSL to DTI-TK"
        echo ""

 		fsl_to_dtitk ${BASENAME}

  		mv ${BASENAME}_L1.nii.gz initial_files/${BASENAME}_L1.nii.gz
  		mv ${BASENAME}_L2.nii.gz initial_files/${BASENAME}_L2.nii.gz
  		mv ${BASENAME}_L3.nii.gz initial_files/${BASENAME}_L3.nii.gz
	
 		mv ${BASENAME}_V1.nii.gz initial_files/${BASENAME}_V1.nii.gz
  		mv ${BASENAME}_V2.nii.gz initial_files/${BASENAME}_V2.nii.gz
 		mv ${BASENAME}_V3.nii.gz initial_files/${BASENAME}_V3.nii.gz

  		mv ${BASENAME}_dtitk_nonSPD.nii.gz initial_files/${BASENAME}_dtitk_nonSPD.nii.gz
  		mv ${BASENAME}_dtitk_norm.nii.gz initial_files/${BASENAME}_dtitk_norm.nii.gz
  		mv ${BASENAME}_dtitk_norm_non_outliers.nii.gz initial_files/${BASENAME}_dtitk_norm_non_outliers.nii.gz

		echo ""
       	        echo "Registering subject ${BASENAME} to the ENA50 tensor template"
        	echo ""

 		dti_rigid_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk.nii.gz EDS 4 4 4 0.01

  		dti_affine_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk.nii.gz EDS 4 4 4 0.01 1
	
  		dti_diffeomorphic_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk_aff.nii.gz ENA50_mask.nii.gz 1 6 0.002

done

echo ""
echo "For all the subjects, concatenating the affine and the diffeomorphic registration adn applying it in one step"
echo ""

ls *_dtitk.nii.gz > all_subjects.txt

dti_warp_to_template_group all_subjects.txt ENA50_tensor.nii.gz 0.9 0.9 0.9

while read p; do
	
  BASENAME=` basename ${p} | cut -d '_' -f 1 `

	echo ""
	echo "Creating the DTI derived maps for the subject ${BASENAME}"
	echo ""

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -fa

		mv ${BASENAME}_dtitk_diffeo_fa.nii.gz ${BASENAME}_FA_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -tr

		fslmaths ${BASENAME}_dtitk_diffeo_tr.nii.gz -div 3 ${BASENAME}_MD_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -rd

		mv ${BASENAME}_dtitk_diffeo_rd.nii.gz ${BASENAME}_RD_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -ad

		mv ${BASENAME}_dtitk_diffeo_ad.nii.gz ${BASENAME}_AD_templatespace.nii.gz

		### NODDI maps: If you also have the NODDI maps, rename it as ${BASENAME}_fintra.nii.gz and ${BASENAME}_ODItot.nii.gz and uncomment the following four lines of code.

	        #SVAdjustVoxelspace -in ${BASENAME}_fintra.nii.gz -origin 0 0 0 -out ${BASENAME}_fintra_origin.nii.gz

		#deformationScalarVolume -in ${BASENAME}_fintra_origin.nii.gz -trans ${BASENAME}_dtitk_combined.df.nii.gz -target ENA50_tensor.nii.gz -out ${BASENAME}_fintra_templatespace.nii.gz -vsize 0.9 0.9 0.9

		#SVAdjustVoxelspace -in ${BASENAME}_ODItot.nii.gz -origin 0 0 0 -out ${BASENAME}_ODItot_origin.nii.gz

		#deformationScalarVolume -in ${BASENAME}_ODItot_origin.nii.gz -trans ${BASENAME}_dtitk_combined.df.nii.gz -target ENA50_tensor.nii.gz -out ${BASENAME}_ODItot_templatespace.nii.gz -vsize 0.9 0.9 0.9

done <all_subjects.txt

echo ""
echo "Creating the stats folder and the concatenated files"
echo ""

mkdir stats/

fslmerge -t stats/all_FA.nii.gz *_FA_templatespace.nii.gz

fslmerge -t stats/all_MD.nii.gz *_MD_templatespace.nii.gz

fslmerge -t stats/all_RD.nii.gz *_RD_templatespace.nii.gz

fslmerge -t stats/all_AD.nii.gz *_AD_templatespace.nii.gz

### NODDI maps: Uncomment the following two lines of code (taking into account that you didn't change the name in the previous NODDI lines).

#fslmerge -t stats/all_fintra.nii.gz *_fintra_templatespace.nii.gz

#fslmerge -t stats/all_ODItot.nii.gz *_ODItot_templatespace.nii.gz

cp ENA50_mask.nii.gz stats/.
cp ENA50_skeleton_mask.nii.gz stats/.
cp ENA50_LowerCingulum.nii.gz stats/.
cp ENA50_FA.nii.gz stats/.
cp ENA50_skeleton_mask_psmd.nii.gz stats/.


cd stats/

echo ""
echo "creating skeleton distancemap (for use in projection search)"
echo ""

# In the original work, we didn't use this step. This creates a mask were all the subjects have a value >0. This step is performed in the original TBSS implementation.
# If you want to do this, uncomment the following 2 lines
#fslmaths all_FA -max 0 -Tmin -bin ENA50_mask.nii.gz -odt char
#fslmaths all_FA -mas ENA50_mask.nii.gz all_FA

fslmaths ENA50_FA.nii.gz -mas ENA50_mask.nii.gz ENA50_FA.nii.gz

fslmaths ENA50_FA.nii.gz -bin ENA50_mask.nii.gz

fslmaths all_FA -mas ENA50_mask.nii.gz all_FA

fslmaths ENA50_mask.nii.gz -mul -1 -add 1 -add ENA50_skeleton_mask ENA50_skeleton_mask_dst

distancemap -i ENA50_skeleton_mask_dst -o ENA50_skeleton_mask_dst

echo ""
echo "projecting all FA data onto skeleton"
echo ""

tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_FA_skeletonised

fslmaths all_MD -mas ENA50_mask all_MD
fslmaths all_RD -mas ENA50_mask all_RD
fslmaths all_AD -mas ENA50_mask all_AD

### NODDI maps: Uncomment the following two lines of code.

#fslmaths all_fintra -mas ENA50_mask all_fintra
#fslmaths all_ODItot -mas ENA50_mask all_ODItot

echo ""
echo "projecting modalities onto mean FA skeleton"
echo ""

tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_MD_skeletonised -a all_MD
tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_RD_skeletonised -a all_RD
tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_AD_skeletonised -a all_AD

### NODDI maps: Uncomment the following two lines of code.

#tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_fintra_skeletonised -a all_fintra
#tbss_skeleton -i ENA50_FA -p 0.15 ENA50_skeleton_mask_dst ENA50_LowerCingulum.nii.gz all_FA all_ODItot_skeletonised -a all_ODItot

cd ..