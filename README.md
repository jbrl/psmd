# Neonatal PSMD

In this repository you can find the code to calculate the peak width of skeletonized water diffusion MRI derived maps in the neonatal brain.

**Important: this instructions are for the updated code. The original implementation can be found in the Original folder**

For the code to work, you need the following softwares:
*  [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki)
*  [DTI-TK](http://dti-tk.sourceforge.net/pmwiki/pmwiki.php)

After pre-processing your data, you need to calculate the DTI maps with FSL (`dtifit`). Then in your study folder, copy all the `ID_L*.nii.gz` and `ID_V*.nii.gz` obtained. Finally, download the code and the atlas (from [here](https://git.ecdf.ed.ac.uk/jbrl/ena/tree/master/ENA50)), and inside the same folder, copy the script and the following modalities of the ENA50:
*  `ENA50_tensor.nii.gz`
*  `ENA50_mask.nii.gz`
*  `ENA50_LowerCingulum.nii.gz`
*  `ENA50_FA.nii.gz`
*  `ENA50_skeleton_mask.nii.gz`
*  `ENA50_skeleton_mask_psmd.nii.gz`

Now everything is ready to go. 

*  First you need to run the following to obtain all the registrations to the template space: `./neonatal_PSMD_1.sh *_L1.nii.gz`.
*  Finally, to obtain the skeletonized files, you just need to run the second script in the same folder:  `./neonatal_PSMD_2.sh all_subjects.txt`.

If you have also NODDI files, rename it as: `ID_fintra.nii.gz` and `ID_ODItot.nii.gz`, copy it in the same folder of the study and uncomment the commented lines of the code.

The final output will be a folder called `skeletonized/`. This folder contains all the skeletonised files, where you can calculate the PSMD (or any desired metric) the following code should be used: 
`fslstats ID_MD_skeletonised.nii.gz -k ENA50_skeleton_mask_psmd.nii.gz -n -P 95` 
to calculate the 95th percentile and 
`fslstats ID_MD_skeletonised.nii.gz -k ENA50_skeleton_mask_psmd.nii.gz -n -P 5` 
for the 5th percentile. Then you only have to do the 95 minus the 5 and that is your value for the PSMD.

You can easily check the order of your subjects by doing `ls *_dtitk.nii.gz` or `imglob *_dtitk.nii.gz` in the main folder.

## Notes

*  This framework (as it is) really benefits of having an accurate masks of the diffusion images. If the masks are not good enough, the registration is likely to perform poorly. If you don't have a good mask, maybe you want to consider changing the registration algorithm. You can read about the impact of this two points. 

## Referencing

If you use this pipeline cite at least the following articles:

The main pipeline and the ENA50 atlas:

*  Blesa, M., Galdi, P., Sullivan, G., Wheater, E. N., Stoye, D. Q., Lamb, G. J., Quigley, A. J., Thrippleton, M. J., Bastin, M. E. and Boardman, J. P. _Submmited_. Peak width of skeletonized water diffusion MRI in the neonatal brain.

The registration algorithm:

*  Zhang,  H.,  Yushkevich,  P. A.,  Alexander,  D. C.,  and Gee,  J. C. (2006). *Deformable registration of diffusion tensor mr images with explicit orientation optimization*. Medical Image Analysis 10, 764 – 785. doi:https://doi.org/10.1016/j.media.2006.06.004. The Eighth International Conference on Medical Imaging and Computer Assisted Intervention – MICCAI 2005

Skeletonization of the data (and if you decide to permform TBSS):

*  Smith, S. M., Jenkinson, M., Johansen-Berg, H., Rueckert, D., Nichols, T. E., Mackay, C. E., et al. (2006). *Tract-based spatial statistics: Voxelwise analysis of multi-subject diffusion data*. NeuroImage 31, 1487-1505. doi:https://doi.org/10.1016/j.neuroimage.2006.02.024

The main PSMD paper:

*  Baykara, E., Gesierich, B., Adam, R., Tuladhar, A. M., Biesbroek, J. M., Koek, H. L., et al. (2016). *A novel imaging marker for small vessel disease based on skeletonization of white matter tracts and diffusion histograms*. Annals of Neurology 80, 581–592. doi:https://doi.org/10.1002/ana.24758

## Contact:

For any doubt/suggestion please email me to: manuel.blesa@ed.ac.uk