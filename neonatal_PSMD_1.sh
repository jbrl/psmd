#!/bin/sh

Usage() {
    echo ""
    echo "Usage"
    echo "neonatal_PSMD_1.sh input"
    echo ""
    echo "Example"
    echo "neonatal_PSMD_1.sh *_L1.nii.gz"
    echo ""
    echo "This script runs the registrations needed for TBSS and PSMD analysis in the neonatal brain"
    echo ""
    echo "Script created by Manuel Blesa"
    echo ""
    exit
}
[ "$1" = "" ] && Usage

mkdir initial_files/

IMAGESET=${*}
for IMG in $IMAGESET
	do

	BASENAME=` basename ${IMG} | cut -d '_' -f 1 `
	echo ""
        echo "Converting subject ${BASENAME} from FSL to DTI-TK"
        echo ""

 		fsl_to_dtitk ${BASENAME}

  		mv ${BASENAME}_L1.nii.gz initial_files/${BASENAME}_L1.nii.gz
  		mv ${BASENAME}_L2.nii.gz initial_files/${BASENAME}_L2.nii.gz
  		mv ${BASENAME}_L3.nii.gz initial_files/${BASENAME}_L3.nii.gz
	
 		mv ${BASENAME}_V1.nii.gz initial_files/${BASENAME}_V1.nii.gz
  		mv ${BASENAME}_V2.nii.gz initial_files/${BASENAME}_V2.nii.gz
 		mv ${BASENAME}_V3.nii.gz initial_files/${BASENAME}_V3.nii.gz

  		mv ${BASENAME}_dtitk_nonSPD.nii.gz initial_files/${BASENAME}_dtitk_nonSPD.nii.gz
  		mv ${BASENAME}_dtitk_norm.nii.gz initial_files/${BASENAME}_dtitk_norm.nii.gz
  		mv ${BASENAME}_dtitk_norm_non_outliers.nii.gz initial_files/${BASENAME}_dtitk_norm_non_outliers.nii.gz

		echo ""
       	        echo "Registering subject ${BASENAME} to the ENA50 tensor template"
        	echo ""

 		dti_rigid_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk.nii.gz EDS 4 4 4 0.01

  		dti_affine_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk.nii.gz EDS 4 4 4 0.01 1
	
  		dti_diffeomorphic_reg ENA50_tensor.nii.gz ${BASENAME}_dtitk_aff.nii.gz ENA50_mask.nii.gz 1 6 0.002

done

echo ""
echo "For all the subjects, concatenating the affine and the diffeomorphic registration adn applying it in one step"
echo ""

ls *_dtitk.nii.gz > all_subjects.txt

dti_warp_to_template_group all_subjects.txt ENA50_tensor.nii.gz 0.9 0.9 0.9

while read p; do
	
  BASENAME=` basename ${p} | cut -d '_' -f 1 `

	echo ""
	echo "Creating the DTI derived maps for the subject ${BASENAME}"
	echo ""

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -fa

		mv ${BASENAME}_dtitk_diffeo_fa.nii.gz ${BASENAME}_FA_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -tr

		fslmaths ${BASENAME}_dtitk_diffeo_tr.nii.gz -div 3 ${BASENAME}_MD_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -rd

		mv ${BASENAME}_dtitk_diffeo_rd.nii.gz ${BASENAME}_RD_templatespace.nii.gz

		TVtool -in ${BASENAME}_dtitk_diffeo.nii.gz -ad

		mv ${BASENAME}_dtitk_diffeo_ad.nii.gz ${BASENAME}_AD_templatespace.nii.gz

		### NODDI maps: If you also have the NODDI maps, rename it as ${BASENAME}_fintra.nii.gz and ${BASENAME}_ODItot.nii.gz and uncomment the following four lines of code.

	        #SVAdjustVoxelspace -in ${BASENAME}_fintra.nii.gz -origin 0 0 0 -out ${BASENAME}_fintra_origin.nii.gz

		#deformationScalarVolume -in ${BASENAME}_fintra_origin.nii.gz -trans ${BASENAME}_dtitk_combined.df.nii.gz -target ENA50_tensor.nii.gz -out ${BASENAME}_fintra_templatespace.nii.gz -vsize 0.9 0.9 0.9

		#SVAdjustVoxelspace -in ${BASENAME}_ODItot.nii.gz -origin 0 0 0 -out ${BASENAME}_ODItot_origin.nii.gz

		#deformationScalarVolume -in ${BASENAME}_ODItot_origin.nii.gz -trans ${BASENAME}_dtitk_combined.df.nii.gz -target ENA50_tensor.nii.gz -out ${BASENAME}_ODItot_templatespace.nii.gz -vsize 0.9 0.9 0.9

done <all_subjects.txt

