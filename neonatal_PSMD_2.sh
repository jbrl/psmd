#!/bin/bash

Usage() {
    echo ""
    echo "Usage"
    echo "neonatal_PSMD_2.sh"
    echo ""
    echo "This script has to be executed in the same folder as neonatal-PSMD_1.sh"
    echo ""
    echo "Example"
    echo "neonatal-PSMD_2.sh all_subjects.txt"
    echo ""
    echo "Script created by Manuel Blesa"
    echo ""
    exit
}
[ "$1" = "" ] && Usage

mkdir skeletonized/

# ITERATE FOR ALL SUBJECTS IN THE TXT FILE

while read p; do

  BASENAME=` basename ${p} | cut -d '_' -f 1 `

  echo ${BASENAME}

  cd skeletonized/

  fslmerge -t all_FA ../${BASENAME}_FA_templatespace.nii.gz
  fslmaths all_FA -max 0 -Tmin -bin mean_FA_mask -odt char
  fslmaths all_FA -mas mean_FA_mask all_FA

  fslmaths ../ENA50_FA -mas mean_FA_mask mean_FA
  fslmaths mean_FA -bin mean_FA_mask
  fslmaths all_FA -mas mean_FA_mask all_FA
  cp ../ENA50_skeleton_mask.nii.gz mean_FA_skeleton_mask.nii.gz

  fslmaths mean_FA_mask -mul -1 -add 1 -add mean_FA_skeleton_mask mean_FA_skeleton_mask_dst
  distancemap -i mean_FA_skeleton_mask_dst -o mean_FA_skeleton_mask_dst

  tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_FA_skeletonised

  fslmerge -t all_MD ../${BASENAME}_MD_templatespace.nii.gz
  fslmaths all_MD -mas mean_FA_mask all_MD
  tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_MD_skeletonised -a all_MD

  fslmerge -t all_RD ../${BASENAME}_RD_templatespace.nii.gz
  fslmaths all_RD -mas mean_FA_mask all_RD
  tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_RD_skeletonised -a all_RD

  fslmerge -t all_AD ../${BASENAME}_AD_templatespace.nii.gz
  fslmaths all_AD -mas mean_FA_mask all_AD
  tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_AD_skeletonised -a all_AD
  
  ### Uncomment the following lines to add the NODDI metrics

  #fslmerge -t all_ODItot ../${BASENAME}_ODItot_templatespace.nii.gz
  #fslmaths all_ODItot -mas mean_FA_mask all_ODItot
  #tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_ODItot_skeletonised -a all_ODItot

  #fslmerge -t all_fintra ../${BASENAME}_fintra_templatespace.nii.gz
  #fslmaths all_fintra -mas mean_FA_mask all_fintra
  #tbss_skeleton -i mean_FA -p 0.15 mean_FA_skeleton_mask_dst ../ENA50_LowerCingulum.nii.gz all_FA ${BASENAME}_fintra_skeletonised -a all_fintra

  rm all_FA.nii.gz
  rm all_MD.nii.gz
  rm all_RD.nii.gz
  rm all_AD.nii.gz
  ### Uncomment the following lines to add the NODDI metrics
  #rm all_ODItot.nii.gz
  #rm all_fintra.nii.gz
  rm mean_FA_mask.nii.gz
  rm mean_FA.nii.gz
  rm mean_FA_skeleton_mask.nii.gz
  rm mean_FA_skeleton_mask_dst.nii.gz

  cd ..

done <all_subjects.txt

cd skeletonized/

fslmerge -t all_FA_skeletonised.nii.gz *_FA_skeletonised.nii.gz
fslmerge -t all_MD_skeletonised.nii.gz *_MD_skeletonised.nii.gz
fslmerge -t all_RD_skeletonised.nii.gz *_RD_skeletonised.nii.gz
fslmerge -t all_AD_skeletonised.nii.gz *_AD_skeletonised.nii.gz
### Uncomment the following lines for the NODDI metrics
#fslmerge -t all_fintra_skeletonised.nii.gz *_fintra_skeletonised.nii.gz
#fslmerge -t all_ODItot_skeletonised.nii.gz *_ODItot_skeletonised.nii.gz

cd ..